package camera.vlad.ua.camera.domain.repository

import android.hardware.Camera
import camera.vlad.ua.camera.data.model.FlashMode

interface CameraRepository {

    var onCameraChanged: (camera: Camera) -> Unit

    var onCamerasRetrieved: (count: Int) -> Unit

    var onFlashModeChanged: (FlashMode) -> Unit

    var onPhotoSaved: (path: String)  -> Unit

    var onPhotoSavedFail: () -> Unit

    fun start()

    fun stop()

    fun changeCamera()

    fun makePhoto()

    fun changeFlashMode()

}