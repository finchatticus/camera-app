package camera.vlad.ua.camera.presentation.base

interface BasePresenter {

    fun start()

    fun stop()

}
