package camera.vlad.ua.camera.data.executor

import android.os.Handler
import android.os.Looper
import camera.vlad.ua.camera.domain.executor.MainThread

/**
 * Created by vlad on 25.04.2018
 */
class MainThreadImpl private constructor() : MainThread {

    private val handler: Handler = Handler(Looper.getMainLooper())

    override fun post(runnable: () -> Unit) {
        handler.post(runnable)
    }

    companion object {

        private var mainThread: MainThread? = null

        val instance: MainThread?
            get() {
                if (mainThread == null) {
                    mainThread = MainThreadImpl()
                }

                return mainThread
            }
    }

}

