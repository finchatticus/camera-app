package camera.vlad.ua.camera.domain.executor

/**
 * Created by vlad on 25.04.2018
 */
interface MainThread {

    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    fun post(runnable: () -> Unit)

}
