package camera.vlad.ua.camera.data.model

/**
 * Created by vlad on 25.04.2018
 */
enum class FlashMode {
    NONE, OFF, ON, AUTO
}