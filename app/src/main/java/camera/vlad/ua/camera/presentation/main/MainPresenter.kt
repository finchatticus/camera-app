package camera.vlad.ua.camera.presentation.main

import android.Manifest
import android.content.pm.PackageManager
import camera.vlad.ua.camera.data.model.FlashMode
import camera.vlad.ua.camera.domain.repository.CameraRepository

class MainPresenter(
        private val v: MainContract.View,
        private val cameraRepository: CameraRepository
) : MainContract.Presenter {

    private companion object {
        const val PERMISSION_CAMERA = Manifest.permission.CAMERA
        const val REQUEST_PERMISSION_CAMERA = 0
        const val PERMISSION_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
        const val REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 1
    }

    init {
        v.presenter = this
        cameraRepository.run {
            onCameraChanged = {
                v.onCameraChanged.invoke(it)
            }
            onCamerasRetrieved = {
                if (it == 0)
                    v.hideChangeCamera()
            }
            onFlashModeChanged = {
                when(it) {
                    FlashMode.NONE -> v.hideFlash()
                    FlashMode.ON -> v.showFlashOn()
                    FlashMode.AUTO -> v.showFlashAuto()
                    FlashMode.OFF -> v.showFlashOff()
                }
            }
            onPhotoSaved = {
                v.showPhotoSavedToast(it)
            }
            onPhotoSavedFail = {
            }
        }
    }

    override fun start() {
        if (v.isPermissionGranted(PERMISSION_CAMERA)) {
            cameraRepository.start()
        } else {
            v.requestPermission(PERMISSION_CAMERA, REQUEST_PERMISSION_CAMERA)
        }
    }

    // https://developer.android.com/training/permissions/requesting.html
    override fun permissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION_CAMERA -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    cameraRepository.start()
                } else {
                    v.showCannotShowCameraToast()
                }
                return
            }
            REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    cameraRepository.makePhoto()
                } else {
                    v.showCannotSavePhotoToast()
                }
                return
            }
        }
    }

    override fun stop() {
        cameraRepository.stop()
    }

    override fun changeCamera() {
        cameraRepository.changeCamera()
    }

    override fun makePhoto() {
        if (v.isPermissionGranted(PERMISSION_WRITE_EXTERNAL_STORAGE)) {
            cameraRepository.makePhoto()
        } else {
            v.requestPermission(PERMISSION_WRITE_EXTERNAL_STORAGE, REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE)
        }

    }

    override fun changeFlash() {
        cameraRepository.changeFlashMode()
    }

}