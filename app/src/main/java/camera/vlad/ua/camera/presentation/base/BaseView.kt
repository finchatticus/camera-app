package camera.vlad.ua.camera.presentation.base

interface BaseView<T> {

    var presenter: T?

}
