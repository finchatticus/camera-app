package camera.vlad.ua.camera.presentation.main

import android.hardware.Camera
import camera.vlad.ua.camera.presentation.base.BasePresenter
import camera.vlad.ua.camera.presentation.base.BaseView

interface MainContract {

    interface View : BaseView<Presenter> {

        var onCameraChanged: (camera: Camera) -> Unit

        fun hideChangeCamera()

        fun hideFlash()

        fun showFlashOn()

        fun showFlashAuto()

        fun showFlashOff()

        fun showPhotoSavedToast(path: String)

        fun showCannotShowCameraToast()

        fun showCannotSavePhotoToast()

        fun requestPermission(permission: String, requestCode: Int)

        fun isPermissionGranted(permission: String): Boolean

    }

    interface Presenter : BasePresenter {

        fun permissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)

        fun changeCamera()

        fun makePhoto()

        fun changeFlash()

    }

}