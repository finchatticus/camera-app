package camera.vlad.ua.camera.presentation.main

import android.content.pm.PackageManager
import android.hardware.Camera
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.Toast
import camera.vlad.ua.camera.R
import camera.vlad.ua.camera.data.repository.CameraRepositoryImpl
import camera.vlad.ua.camera.presentation.view.CameraPreview

class MainActivity : AppCompatActivity(), MainContract.View {

    override var presenter: MainContract.Presenter? = null

    private val imageButtonChangeCamera by lazy { findViewById<ImageButton>(R.id.button_change_camera) }
    private val imageButtonMakePhoto by lazy { findViewById<ImageButton>(R.id.button_make_photo) }
    private val imageButtonChangeFlash by lazy { findViewById<ImageButton>(R.id.button_change_flash) }
    private val preview by lazy { findViewById<FrameLayout>(R.id.camera_preview) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainPresenter(this, CameraRepositoryImpl(getString(R.string.app_name))) // init presenter
        imageButtonChangeCamera.setOnClickListener {
            presenter?.changeCamera()
        }
        imageButtonMakePhoto.setOnClickListener {
            presenter?.makePhoto()
        }
        imageButtonChangeFlash.setOnClickListener {
            presenter?.changeFlash()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter?.start()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        presenter?.permissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onPause() {
        super.onPause()
        presenter?.stop()
    }

    override var onCameraChanged: (camera: Camera) -> Unit = {
        val cameraPreview = CameraPreview(this, it)
        preview.removeAllViews()
        preview.addView(cameraPreview)
    }

    override fun hideChangeCamera() {
        imageButtonChangeCamera.visibility = View.GONE
    }

    override fun hideFlash() {
        imageButtonChangeFlash.visibility = View.GONE
    }

    override fun showFlashOn() {
        showFlash()
        imageButtonChangeFlash.setImageResource(R.drawable.ic_flash_on)
    }

    override fun showFlashAuto() {
        showFlash()
        imageButtonChangeFlash.setImageResource(R.drawable.ic_flash_auto)
    }

    override fun showFlashOff() {
        showFlash()
        imageButtonChangeFlash.setImageResource(R.drawable.ic_flash_off)
    }

    override fun showPhotoSavedToast(path: String) {
        Toast.makeText(this, getString(R.string.photo_saved, path), Toast.LENGTH_SHORT).show()
    }

    override fun showCannotShowCameraToast() {
        Toast.makeText(this, getString(R.string.cannot_show_camera), Toast.LENGTH_LONG).show()
    }

    override fun showCannotSavePhotoToast() {
        Toast.makeText(this, getString(R.string.cannot_save_photo), Toast.LENGTH_LONG).show()
    }

    override fun requestPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {*/
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(permission),
                        requestCode)
//            }
        }
    }

    override fun isPermissionGranted(permission: String) =
            ContextCompat.checkSelfPermission(this@MainActivity, permission) == PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP

    private inline fun showFlash() {
        imageButtonChangeFlash.visibility = View.VISIBLE
    }

}
